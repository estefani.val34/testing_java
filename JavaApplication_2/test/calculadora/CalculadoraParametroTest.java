/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculadora;

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

/**
 *
 * @author portatil
 */

@RunWith(value=Parameterized.class)
public class CalculadoraParametroTest {
    
   @Parameters
   public static Iterable<Object[]> getData(){
       List <Object[]> obj=new ArrayList<>();
       obj.add(new Object[]{3,1,4});
       obj.add(new Object[]{4,1,5});
       obj.add(new Object[]{3,6,9});
       return obj;
       
   }
    
    
    
    private int a;
    private int b;
    private int exp;
    public CalculadoraParametroTest(int a, int b, int exp) {
        this.a=a;
        this.b=b;
        this.exp=exp;
    }
    
    

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
     @Test
     public void testAdd() {
     Calculadora calc=new Calculadora();
     int result =calc.add(a,b);
     assertEquals(exp, result);
     }
}
