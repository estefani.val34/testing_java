package pokemons;

import java.util.Objects;
/**
 * Class pokemon contains every var of the pokemon and some methods
 * @author medel
 */
public class Pokemon {
    
    private String Pok_id;
    private String nombre;
    private String tipo1;
    private String tipo2;
    private int nivel;
    private int energiaAct;
    private int energiaMax;
    private double ataque;
    private double defensa;
    private double velocidad;
    private double defensaEsp;
    private double ataqueEsp;

    /**
     * Constructor
     * @param Pok_id
     * @param nombre
     * @param tipo1
     * @param tipo2
     * @param nivel
     * @param energiaAct
     * @param energiaMax
     * @param ataque
     * @param defensa
     * @param velocidad
     * @param defensaEsp
     * @param ataqueEsp 
     */
    public Pokemon(String Pok_id, String nombre, String tipo1, String tipo2, int nivel, int energiaAct, int energiaMax, double ataque, double defensa, double velocidad, double defensaEsp, double ataqueEsp) {
        this.Pok_id = Pok_id;
        this.nombre = nombre;
        this.tipo1 = tipo1;
        this.tipo2 = tipo2;
        this.nivel = nivel;
        this.energiaAct = energiaAct;
        this.energiaMax = energiaMax;
        this.ataque = ataque;
        this.defensa = defensa;
        this.velocidad = velocidad;
        this.defensaEsp = defensaEsp;
        this.ataqueEsp = ataqueEsp;
    }

    /**
     * method toString (Overrided)
     * @return 
     */
    @Override
    public String toString() {
        return "Pokemon{" + "Pok_id=" + Pok_id + ", nombre=" + nombre + ", tipo1=" + tipo1 + ", tipo2=" + tipo2 + ", nivel=" + nivel + ", energiaAct=" + energiaAct + ", energiaMax=" + energiaMax + ", ataque=" + ataque + ", defensa=" + defensa + ", velocidad=" + velocidad + ", defensaEsp=" + defensaEsp + ", ataqueEsp=" + ataqueEsp + '}';
    }

    /**
     * method hashcode (Overrided)
     * @return 
     */
    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    /**
     * method equals (Overrided)
     * @param obj
     * @return 
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Pokemon other = (Pokemon) obj;
        if (!Objects.equals(this.Pok_id, other.Pok_id)) {
            return false;
        }
        if (!Objects.equals(this.tipo1, other.tipo1)) {
            return false;
        }
        if (!Objects.equals(this.tipo2, other.tipo2)) {
            return false;
        }
        return true;
    }

    
    /**
     * get pokemon_id
     * @return pok_id
     */
    public String getPok_id() {
        return Pok_id;
    }

    /**
     * set pokemon_id
     * @param Pok_id 
     */
    public void setPok_id(String Pok_id) {
        this.Pok_id = Pok_id;
    }

    /**
     * getNombre
     * @return nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * set nombre
     * @param nombre 
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * getTipo1
     * @return tipo1
     */
    public String getTipo1() {
        return tipo1;
    }

    /**
     * setTipo1
     * @param tipo1 
     */
    public void setTipo1(String tipo1) {
        this.tipo1 = tipo1;
    }

    /**
     * getTipo2
     * @return tipo2
     */
    public String getTipo2() {
        return tipo2;
    }

    /**
     * set tipo2
     * @param tipo2 
     */
    public void setTipo2(String tipo2) {
        this.tipo2 = tipo2;
    }

    /**
     * get nivel
     * @return nivel
     */
    public int getNivel() {
        return nivel;
    }

    /**
     * set nivel
     * @param nivel 
     */
    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    /**
     * get EnergiaAct
     * @return energiaAct
     */
    public int getEnergiaAct() {
        return energiaAct;
    }

    /**
     * set EnergiaAct
     * @param energiaAct 
     */
    public void setEnergiaAct(int energiaAct) {
        this.energiaAct = energiaAct;
    }

    /**
     * getEnergiaMax
     * @return energiaMax
     */
    public int getEnergiaMax() {
        return energiaMax;
    }
    /**
     * set energiaMax
     * @param energiaMax 
     */
    public void setEnergiaMax(int energiaMax) {
        this.energiaMax = energiaMax;
    }

    /**
     * getAtaque
     * @return ataque
     */
    public double getAtaque() {
        return ataque;
    }

    /**
     * setAtaque
     * @param ataque 
     */
    public void setAtaque(double ataque) {
        this.ataque = ataque;
    }

    /**
     * getDefensa
     * @return defensa
     */
    public double getDefensa() {
        return defensa;
    }

    /**
     * setDefensa
     * @param defensa 
     */
    public void setDefensa(double defensa) {
        this.defensa = defensa;
    }

    /**
     * getVelocidad
     * @return velocidad
     */
    public double getVelocidad() {
        return velocidad;
    }

    /**
     * setVelocidad
     * @param velocidad 
     */
    public void setVelocidad(double velocidad) {
        this.velocidad = velocidad;
    }

    /**
     * getDefensaEsp
     * @return defensaEsp
     */
    public double getDefensaEsp() {
        return defensaEsp;
    }

    /**
     * setDefensaEsp
     * @param defensaEsp 
     */
    public void setDefensaEsp(double defensaEsp) {
        this.defensaEsp = defensaEsp;
    }

    /**
     * getAtaqueEsp
     * @return ataqueEsp
     */
    public double getAtaqueEsp() {
        return ataqueEsp;
    }

    /**
     * set ataqueEsp
     * @param ataqueEsp 
     */
    public void setAtaqueEsp(double ataqueEsp) {
        this.ataqueEsp = ataqueEsp;
    }    
}
