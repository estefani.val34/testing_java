
package ejercicio2;

/**
 * 
 * @author jimmy
 */
public class Ejercicio2 {


    public static void main(String[] args) {
        
        /*Genera todos los pokemons de prueba*/
        Pokemon p1 = new Pokemon("1a2x3c", "Bulbasaur", "agua", "tierra", 34, 100, 1000, 1.4, 1.3, 0.4, 5.5, 3.4);
        Pokemon p2 = new Pokemon("d45gt", "Ivysaur", "tierra", "aire", 34, 100, 1000, 1.4, 1.3, 0.4, 5.5, 3.4);
        Pokemon p3 = new Pokemon("5y6fd", "Venusaur", "agua", "-", 34, 100, 1000, 1.4, 1.3, 0.4, 5.5, 3.4);
        Pokemon p4 = new Pokemon("e56jy", "Charmander", "aire", "-", 34, 100, 1000, 1.4, 1.3, 0.4, 5.5, 3.4);
        Pokemon p5 = new Pokemon("eee34", "Charmeleon", "agua", "tierra", 34, 100, 1000, 1.4, 1.3, 0.4, 5.5, 3.4);
        
        /*Imprimimos con el toString todos los pokemons con su informacion*/
        System.out.println(p1.toString());
        System.out.println(p2.toString());
        System.out.println(p3.toString());
        System.out.println(p4.toString());
        System.out.println(p5.toString());
        
    }
    
}
