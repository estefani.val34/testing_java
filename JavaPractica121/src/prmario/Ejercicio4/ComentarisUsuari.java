/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ejercicio4;

/*@param Se realiza los valores para luego introducir los datos*/
public class ComentarisUsuari {

    private String nom;
    private String email;
    private String web;
    private String comentari;

    /*@param Se crea un constructor vacio*/
    public ComentarisUsuari() {
    }

    /*@param Se crea un contructor con todos los valores para introducir*/
    public ComentarisUsuari(String nom, String email, String web, String comentari) {
        this.nom = nom;
        this.email = email;
        this.web = web;
        this.comentari = comentari;
    }

    /*@return Se realiza los getter y setter para llamar o recoger la informacion*/
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWeb() {
        return web;
    }

    public void setWeb(String web) {
        this.web = web;
    }

    public String getComentari() {
        return comentari;
    }

    public void setComentari(String comentari) {
        this.comentari = comentari;
    }

    /*@return Se crea el tostring para imprimir todo el contenido de la clase*/
    @Override
    public String toString() {
        return "ComentariaUsuari{" + "nom=" + nom + ", email=" + email + ", web=" + web + ", comentari=" + comentari + '}';
    }
}
