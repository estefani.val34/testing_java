package Ex4;

import Ex3.*;
import java.util.Scanner;
import java.io.*;
import java.util.*;
import static java.util.Spliterators.iterator;
import static java.util.Spliterators.iterator;

/**
 * @author Francisco Regaña Corral
 */
public class Comentaris {
    
    
    /**
     * Funció que recull tots els parametres que pertanyen a un comentari. Tambe controla que 
     * els camps siguin correctes segons l'exercici 3 (que està importat).
     * Un cop recollits instancia un objecte ComentarisUsuari amb tots els 
     * parametres anteriorment introduits.
     * Finalment afegeix l'objecte al HashMap
     * @param comentaris És el hashmap on es guardaran els comentaris. La Clau serà
     * el nom de la persona i el valor tot l'objecte comentari.
     */
    public static void insercioNouComentari(HashMap comentaris) {
        String nom, email, web, comentari;
        Scanner intro = new Scanner(System.in).useDelimiter("\n");
        Validador v = new Validador();

        do {
            System.out.println("Si us plau inserta el nom de la persona que ha fet el comentari");
            nom = intro.next();
            if (!v.validaNomMayuscula(nom)) {
                System.out.println("El nom ha d'estar en mayuscules");
            }
        } while (!v.validaNomMayuscula(nom));

        do {
            System.out.println("Si us plau inserta el l'email de la persona que ha fet el comentari");
            email = intro.next();
            if (!v.validaCampEmail(email)) {
                System.out.println("L'email ha de tenir el format correcte");
            }
        } while (!v.validaCampEmail(email));

        do {
            System.out.println("Si us plau inserta de quina direccio web prové el comentari");
            web = intro.next();
            if (!v.validaWeb(web)) {
                System.out.println("El camp web,  ha de tenir el format correcte");
            }
        } while (!v.validaWeb(web));

        do {
            System.out.println("Si us plau inserta El comentari");
            comentari = intro.next();
            if (!v.validaCampNoBuit(comentari)) {
                System.out.println("El camp comentari no pot estar buit");
            }
            if (!v.validaNoParaulesOfensives(comentari)) {
                System.out.println("No es permet l'us de paraules ofensives");
            }
        } while (!v.validaCampNoBuit(comentari) && !v.validaNoParaulesOfensives(comentari));

        ComentarisUsuari p = new ComentarisUsuari(nom, email, web, comentari);

        comentaris.put(p.getNom(), p);
    }
    /**
     * Procediment que demana a l'usuari quin es el nom de la persona que ha escrit
     * el comentari. Desprès busca si algun component del HashMap conte alguna clau
     * amb el nom inserit. Si el troba l'imprimeix
     * @param comentaris Hashmap amb tots els comentaris. CLAU: nom || VALOR: Comentari (objecte).
     */
    public static void imprimeixComentari(HashMap comentaris) {
        Scanner intro = new Scanner(System.in);
        System.out.println("Esciu el nom de la persona que ha escrit el comentari que vols llegir: ");
        String nom = intro.nextLine();
        
        if(comentaris.containsKey(nom)){
            System.out.println(comentaris.get(nom).toString());

        }
            System.out.println("");
    }
    /**
     * Funció que recórre tots els componens del HashMap i el imprimeix.
     * @param llistat És el HashMap en cuestio
     */
    public static void imprimeixTotaLaLlista(HashMap <String, ComentarisUsuari> llistat) {
         
        llistat.forEach((k,v) -> System.out.printf("[" + k.toString().toUpperCase() + ": " + v.toString() + "]\n"));
        System.out.println("");
    }

    public static void menu(HashMap comentaris, int opcio) {
        
        switch (opcio) {
            case 0:
                System.out.println("Fins aviat");
                break;
            case 1:
                insercioNouComentari(comentaris);
                break;
            case 2:
                imprimeixComentari(comentaris);
                break;
            case 3:
                imprimeixTotaLaLlista(comentaris);
                break;
        }
         
    }

    public static void main(String[] args) {
        Scanner intro = new Scanner(System.in);
        ComentarisUsuari[] comentaris = new ComentarisUsuari[20];
        HashMap <String, ComentarisUsuari> llistat = new HashMap <String, ComentarisUsuari> ();
        load(llistat);
        int opcio = 0;
        
        
        do{
            System.out.println("");
            System.out.println("Indica que vols fer: ");
            System.out.println("0.- Sortir");
            System.out.println("1.- Insereix un nou comentari");
            System.out.println("2.- Imprimeix un comentari");
            System.out.println("3.- imprimeix tots els comentaris");
            opcio = intro.nextInt();
            menu(llistat, opcio);
        }while (opcio == 1 || opcio == 2 || opcio == 3);

    }
    /**
     * Procediment que instancia objectes de tipus ComentarisUsuari
     * per carregar-los al HashMap del menu principel.
     * 
     * @param llistat Serà el HashMap on es carregaran tots el objectes de la clase
     * ComentarisUsuari com a valor, el nom serà la clau.
     */
    public static void load(HashMap <String, ComentarisUsuari> llistat){
        //String nom, String email, String web, String comentari
        
        ComentarisUsuari c1 = new ComentarisUsuari("Jordi", "idroj@hotmail.com", " https://netbeans.apache.org/", "Hola em dic Jordi i m'encanta comentar frases");
        ComentarisUsuari c2 = new ComentarisUsuari("Laia", "qqqq@hotmail.com", " https://google/", "És una injusticia aixo que dius");
        ComentarisUsuari c3 = new ComentarisUsuari("Francisco", "gako22@hotmail.com", " https://xarxes.apache.com/", "L'email no em funciona be");
        ComentarisUsuari c4 = new ComentarisUsuari("Michael", "Mich@mailinator.es", " https://nose.sise.org/", "Nomes se que no en se res");
        ComentarisUsuari c5 = new ComentarisUsuari("Jackson", "jacksonfive@outlook.com", " https://viva.lavida.org/", "Viu i deixa viure");
        ComentarisUsuari c6 = new ComentarisUsuari("Trini", "trini@yopis.es", " https://netbeans.apache.org/", "Hola em dic Jordi i m'encanta comentar frases");
        ComentarisUsuari c7 = new ComentarisUsuari("Baldufa", "bal@hotmail.com", " https://netbeans.nose.org/", "No s'amacudeixen ja mes comentaris");
        ComentarisUsuari c8 = new ComentarisUsuari("MariChoni", "choniMari@hotmail.com", " https://beans.nose.org/", "Tens sort de que no espoden utilitzar paraulotes perque et posava bullint...");
        ComentarisUsuari c9 = new ComentarisUsuari("Catalina", "llibertatalspresos@hotmail.com", " https://fem.republica.org/", "Vinga, a l'escola Marichoni");
        
        llistat.put(c1.getNom(), c1);
        llistat.put(c2.getNom(), c2);
        llistat.put(c3.getNom(), c3);
        llistat.put(c4.getNom(), c4);
        llistat.put(c5.getNom(), c5);
        llistat.put(c6.getNom(), c6);
        llistat.put(c7.getNom(), c7);
        llistat.put(c8.getNom(), c8);
        llistat.put(c9.getNom(), c9);
    }
}