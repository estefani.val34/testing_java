package javaapplication4;



/**
 * 23. Crea una classe Validador que servirà per verificar camps de formularis;
 * i una classe Main que provi tots els mètodes. Aquests seran: 
 * -validaCampNoBuit(String camp) 
 * -validaTamanyMaximCamp(String camp, int tamanyMaxCamp) 
 * -validaCampEmail(String camp) 
 * - validaIntervalNumero(int intMinim, int intMaxim)
 */
public class  validator_campos{

    static final String SI = "Sí";
    static final String NO = "No";
    static final String CAMP_NO_BUIT = "El camp està informat?";
    static final String CAMP_PRIM_MAJUSCULES = "Primera lletra majuscula ?";
    static final String CAMP_INTERNVAL = "El camp sobrepassa l'interval indicat?";
    static final String CAMP_EMAIL = "El camp és un email vàlid?";
    static final String CAMP_URL = "El camp és una adreça web vàlida?";
    
    
    private static void imprimeixResposta(boolean respostaAfirmativa) {
        if(respostaAfirmativa)
            System.out.println(SI);
        else
            System.out.println(NO);
    }
    
    public static void main(String[] args) {
       //1 – Validar que cap dels camps de text requerits (inclòs el camp de comentaris) sigui correcte. 
        Validator validador = new Validator();
        System.out.println(CAMP_NO_BUIT);
        imprimeixResposta(validador.validaCampNoBuit("Victor")); 
        System.out.println(CAMP_NO_BUIT);
        imprimeixResposta(validador.validaCampNoBuit("")); 
        System.out.println(CAMP_NO_BUIT);
        imprimeixResposta(validador.validaCampNoBuit(null)); 
       //2 – Validar que el campo no nombre empieza unicamente con una lletra en majúscules.       
        System.out.println(CAMP_PRIM_MAJUSCULES);
        imprimeixResposta(validador.validaPrimeraLletraMajuscules("Victor")); 
         System.out.println(CAMP_PRIM_MAJUSCULES);
        imprimeixResposta(validador.validaPrimeraLletraMajuscules("victor")); 
        System.out.println(CAMP_PRIM_MAJUSCULES);
        imprimeixResposta(validador.validaPrimeraLletraMajuscules("vIctor")); 
        System.out.println(CAMP_PRIM_MAJUSCULES);
        imprimeixResposta(validador.validaPrimeraLletraMajuscules("")); 
       // 3 – Validar que els camps de texto 1,2 i 3 no superin els 40 caràcters de llarg.  
        System.out.println(CAMP_INTERNVAL);
        imprimeixResposta(validador.validaTamanyMaximCamp("Nombre",40));
        System.out.println(CAMP_INTERNVAL);
        imprimeixResposta(validador.validaTamanyMaximCamp("456789",40));
         System.out.println(CAMP_INTERNVAL);
        imprimeixResposta(validador.validaTamanyMaximCamp("456789101112131415161718192021222325232323232323435436546313544234412333243",40));  
        // 4 – Validar que el campo de texto 4 (comentaris), no superi els 140 caràcters de llarg
          System.out.println(CAMP_INTERNVAL);
        imprimeixResposta(validador.validaTamanyMaximCamp("456789101112131415161718192021222325232323232323435436546313544234412333243",140));
        //5– Validar que l’email estigui composada d’una cadena de caràcters amb un format d’email vàlid.
        System.out.println(CAMP_EMAIL);
        imprimeixResposta(validador.validaCampEmail("Nombre"));
        System.out.println(CAMP_EMAIL);
        imprimeixResposta(validador.validaCampEmail("vic.castillaabeledo@gmail.com"));
        //6 – Validar que el lloc web es composi d’una cadena de text amb un text separat seguit d’un o més punts.
        System.out.println(CAMP_URL + " Nombre");
        imprimeixResposta(validador.validaCampURL("Nombre"));
        String website1 = "https://www.yahoo.org/";
        System.out.println(CAMP_URL + " " + website1);
        imprimeixResposta(validador.validaCampURL(website1));
        String website2 = "as.es";
        System.out.println(CAMP_URL + " " + website2);
        imprimeixResposta(validador.validaCampURL(website2));
        
        
        
        
   }
  
}