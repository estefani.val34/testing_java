package practica22;

/**
 * Validar que cap dels camps de text requerits (inclòs el camp de comentaris)
 * sigui correcte.
 *
 * @author estefani
 */
public class ProvaValidador {

    public ProvaValidador() {

    }

    //nombre
    /**
     * valida que el campo no este vacío
     * @param camp
     * @return 
     */
    public boolean validaCampNoBuit(String camp) {
        return camp != null || !camp.isEmpty();
    }
    
    //email
    /**
     * valida el email
     * @param email
     * @return 
     */
    public boolean validaCampEmail(String email) {

        String regex = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
        return email.matches(regex);
    }

    // sitio web
    /**
     * valida sitio web
     * @param url
     * @return 
     */
    public boolean validaCampUrl(String url) {
        String regex = "[A-Za-z0-9](([_\\.\\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\\.\\-]?[a-zA-Z0-9]+)*)\\.([A-Za-z]{2,}";
        return url.matches(regex);
    }

    /**
     * Validar que els camps de texto 1,2 i 3 no superin els 40 caràcters de
     * llarg.
     *
     * @param camp
     * @param tamanyMaxCamp
     * @return
     */
    public boolean validaTamanyMaximCamp(String camp, int tamanyMaxCamp) {
        return camp.length() > tamanyMaxCamp;
    }

    /**
     * Validar que el campo no nombre empieza unicamente con una lletra en
     * majúscules.
     *
     * @param camp
     * @return
     */
    public boolean validaPrimeraLetraMAjusculas(String camp) {
        String regex = "[A-Z].*";
        return camp.matches(regex);
    }
    /**
     * valida que no saltan estas palabras [“ximple”,”imbècil”,”babau”,”inútil”,”burro”,”loser”,”noob”,”capsigrany”,”torrecollons”,”fatxa”,”nazi”,”supremacista”]
     * @param camp
     * @return 
     * 
     */
    public boolean validaParaulesProhibides(String camp) {
        String regex = "(?i:.*(ximple|imbecil|babau|inutil|burro|loser|noob|capsigrany|torrecollons|fatxa|nazi|supremacista).*)";
        return camp.matches(regex);
                 
    }
    
    

}
