/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication_1;

import Ej3_FormValidations.Validator;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author portatil
 */
public class FieldsValidatorTest {

    static Validator myValidator;

    public FieldsValidatorTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        myValidator = new Validator();
    }

    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void testCorrectEmail() {
        String testEmail = "miquel@gmail.com";
        boolean expectedResult = true;
        boolean result = myValidator.valEmail(testEmail);
        System.out.println("");
        assertEquals(expectedResult, result);
    }

    @Test
    public void testIncorrectEmail1() {
        String testEmail = "Miquel";
        TestFalseEmail(testEmail);
    }

    @Test
    public void testIncorrectEmailDotCom() {
        String testEmail = "miquel.com";
        TestFalseEmail(testEmail);
    }

    private void TestFalseEmail(String testEmail) {
        boolean expectedResult = false;
        boolean result = myValidator.valEmail(testEmail);
        System.out.println("");
        assertEquals(expectedResult, result);
        // 
    }

   
   //test empty  incorrect 
    @Test
    public void testvalEmpty() {
        String test = "";
        TestFalse(test);
    }

    private void TestFalse(String test) {
        boolean expectedResult = false;
        boolean result = myValidator.valNotEmpty(test);
        System.out.println("");
        assertEquals(expectedResult, result);
        
    }
    //test no empty correct 
   @Test
    public void testCorrectEmpty() {
        String test = "miquel";
        boolean expectedResult = true;
        boolean result = myValidator.valNotEmpty(test);
        System.out.println("");
        assertEquals(expectedResult, result);
    }

    //mayusculas incorrect
    @Test
    public void testvalFirstLetterUppercase() {
        String test = "ana";
        Testincorrect(test);
    }

    private void Testincorrect(String test) {
        boolean expectedResult = false;
        boolean result = myValidator.valFirstLetterUppercase(test);
        System.out.println("");
        assertEquals(expectedResult, result);
    }
    
    //mayusculas correct
    @Test
    public void testCorrectvalFirstLetterUppercase() {
        String test = "Miquel";
        boolean expectedResult = true;
        boolean result = myValidator.valFirstLetterUppercase(test);
        System.out.println("");
        assertEquals(expectedResult, result);
    }
    
    
    
    
}
